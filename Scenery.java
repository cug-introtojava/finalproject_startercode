/**
 * Scenery objects are NON-TAKEABLE items meant to be added to rooms to match things
 * mentioned in a room description. The viewer can "examine" them but not interact with
 * them or pick them up.
 */
public class Scenery extends Item {

	/**
	 * A Scenery object is an NON-TAKEABLE Item.  See description above.
	 */
	public Scenery(World world, String name, int weight, String description) {
		super(world, name, weight, Item.NOT_TAKEABLE, description);
	}

	@Override
	public void doUse() {
		World.print("The " + getName() + " just sits there.  Nothing happens.\n\n");
	}
}
